FROM mcr.microsoft.com/dotnet/sdk:6.0  AS build-env
ARG DB_SERVER
ARG DB_PORT
ARG DB_NAME
ARG DB_USER
ARG DB_PASSWORD
ARG REDIS_HOST
ARG REDIS_INSTANCE_NAME
ARG EMAIL_ADDRESS
ARG EMAIL_SERVER
ARG EMAIL_PORT
ARG EMAIL_USERNAME
ARG EMAIL_PASSWORD

WORKDIR /app
RUN ls
COPY . ./

RUN apt-get update
RUN apt-get install gettext-base
RUN export ASPNETCORE_ENVIRONMENT='Production'
RUN cp ./TaskManagement.API/appsettings.Production.json ./TaskManagement.API/_appsettings.Production.json
RUN envsubst < ./TaskManagement.API/_appsettings.Production.json > ./TaskManagement.API/appsettings.Production.json

RUN dotnet publish -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "TaskManagement.API.dll"]
