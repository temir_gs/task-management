using AutoMapper;
using TaskManagement.API.Users.Application.Dtos;
using TaskManagement.API.Users.Domain.Models;

namespace TaskManagement.API.Users.Application.Mapper;

public class UserMappingProfile : Profile
{
    public UserMappingProfile()
    {
        CreateMap<User, UserDto>();

        CreateMap<UserCreateDto, User>();
        CreateMap<UserCreateDto, EmailValidDto>();
        CreateMap<UserCreateDto, UsernameValidDto>();

        CreateMap<UserUpdateDto, User>();
        CreateMap<UserUpdateDto, EmailValidDto>();
        CreateMap<UserUpdateDto, UsernameValidDto>();
    }
}