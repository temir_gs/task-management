namespace TaskManagement.API.Users.Application.Dtos;

public class UserCreateDto
{
    public string UserName { get; set; }
    public string Password { get; set; }
    public string Email { get; set; }
}