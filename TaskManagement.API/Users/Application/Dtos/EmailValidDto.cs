namespace TaskManagement.API.Users.Application.Dtos;

public class EmailValidDto
{
    public int? Id { get; set; }
    public string Email { get; set; }
}