namespace TaskManagement.API.Users.Application.Dtos;

public class UsernameValidDto
{
    public int? Id { get; set; }
    public string UserName { get; set; }
}