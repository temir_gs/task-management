using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Users.Application.Services;

public interface IUserService
{
    Task<UserDto> GetByIdAsync(int id, int organizationId);
    Task<IList<UserDto>> GetAllAsync(int organizationId);
    Task CreateAsync(UserCreateDto userCreateDto);
    Task UpdateAsync(UserUpdateDto userUpdateDto);
    Task DeleteAsync(int id, int organizationId);
    Task<bool> IsEmailValidAsync(EmailValidDto emailValidDto);
    Task<bool> IsUsernameValidAsync(UsernameValidDto usernameValidDto);
}