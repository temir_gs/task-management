using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using TaskManagement.API.ApplicationDataContext;
using TaskManagement.API.Common.Exceptions;
using TaskManagement.API.Helpers.PasswordHelpers;
using TaskManagement.API.Users.Application.Dtos;
using TaskManagement.API.Users.Domain.Models;

namespace TaskManagement.API.Users.Application.Services;

public class UserService : IUserService
{
    private readonly IMapper _mapper;
    private readonly DataContext _dataContext;

    private readonly IValidator<EmailValidDto> _emailValidator;
    private readonly IValidator<UserUpdateDto> _userUpdateValidator;
    private readonly IValidator<UserCreateDto> _userCreateValidator;
    private readonly IValidator<UsernameValidDto> _usernameValidator;

    public UserService(
        IMapper mapper,
        DataContext dataContext,
        IValidator<EmailValidDto> emailValidator,
        IValidator<UsernameValidDto> usernameValidator,
        IValidator<UserCreateDto> userCreateValidator,
        IValidator<UserUpdateDto> userUpdateValidator)
    {
        _mapper = mapper;
        _dataContext = dataContext;
        _emailValidator = emailValidator;
        _usernameValidator = usernameValidator;
        _userCreateValidator = userCreateValidator;
        _userUpdateValidator = userUpdateValidator;
    }

    public async Task<UserDto> GetByIdAsync(int id, int organizationId)
    {
        User? user = await _dataContext.Users
            .AsNoTracking()
            .FirstOrDefaultAsync(e => e.Id == id && e.OrganizationId == organizationId);

        NotFoundException.ThrowIfNull(user, nameof(id), id);

        return _mapper.Map<UserDto>(user);
    }

    public async Task<IList<UserDto>> GetAllAsync(int organizationId)
    {
        List<User> users = await _dataContext.Users.Where(x => x.OrganizationId == organizationId)
            .AsNoTracking()
            .ToListAsync();

        return _mapper.Map<IList<UserDto>>(users);
    }

    public async Task CreateAsync(UserCreateDto userCreateDto)
    {
        await _userCreateValidator.ValidateAndThrowAsync(userCreateDto);
        await ValidateCreateAsync(userCreateDto);

        User newUser = _mapper.Map<User>(userCreateDto);
        (string passwordHash, string saltHash) = PasswordHelper.CreatePasswordHash(userCreateDto.Password);
        newUser.SaltHash = saltHash;
        newUser.PasswordHash = passwordHash;

        await _dataContext.Users.AddAsync(newUser);
        await _dataContext.SaveChangesAsync();
    }

    public async Task UpdateAsync(UserUpdateDto userUpdateDto)
    {
        await _userUpdateValidator.ValidateAndThrowAsync(userUpdateDto);
        await ValidateUpdateAsync(userUpdateDto);

        User? user = await _dataContext.Users.FirstOrDefaultAsync(e => e.Id == userUpdateDto.Id);

        NotFoundException.ThrowIfNull(user, nameof(userUpdateDto.Id), userUpdateDto.Id);

        _mapper.Map(userUpdateDto, user);

        _dataContext.Users.Update(user);
        await _dataContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id, int organizationId)
    {
        User? user = await _dataContext.Users
            .FirstOrDefaultAsync(e => e.Id == id && e.OrganizationId == organizationId);
        
        NotFoundException.ThrowIfNull(user, nameof(id), id);

        _dataContext.Users.Remove(user);
        await _dataContext.SaveChangesAsync();
    }

    public async Task<bool> IsEmailValidAsync(EmailValidDto emailValidDto)
    {
        await _emailValidator.ValidateAndThrowAsync(emailValidDto);

        bool isExist = await _dataContext.Users.AnyAsync(u =>
            u.Email == emailValidDto.Email &&
            (!emailValidDto.Id.HasValue || u.Id != emailValidDto.Id));

        return !isExist;
    }

    public async Task<bool> IsUsernameValidAsync(UsernameValidDto usernameValidDto)
    {
        await _usernameValidator.ValidateAndThrowAsync(usernameValidDto);

        bool isExist = await _dataContext.Users.AnyAsync(u =>
            u.UserName == usernameValidDto.UserName &&
            (!usernameValidDto.Id.HasValue || u.Id != usernameValidDto.Id));
        return !isExist;
    }

    private async Task ValidateCreateAsync(UserCreateDto userCreateDto)
    {
        EmailValidDto emailValidDto = _mapper.Map<EmailValidDto>(userCreateDto);
        bool isEmailExists = await IsEmailValidAsync(emailValidDto);
        if (!isEmailExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(emailValidDto.Email), emailValidDto.Email);
        }

        UsernameValidDto usernameValidDto = _mapper.Map<UsernameValidDto>(userCreateDto);
        bool isUsernameExists = await IsUsernameValidAsync(usernameValidDto);
        if (!isUsernameExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(usernameValidDto.UserName), usernameValidDto.UserName);
        }
    }

    private async Task ValidateUpdateAsync(UserUpdateDto userUpdateDto)
    {
        EmailValidDto emailValidDto = _mapper.Map<EmailValidDto>(userUpdateDto);
        bool isEmailExists = await IsEmailValidAsync(emailValidDto);
        if (!isEmailExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(emailValidDto.Email),
                emailValidDto.Email);
        }

        UsernameValidDto usernameValidDto = _mapper.Map<UsernameValidDto>(userUpdateDto);
        bool isUsernameExists = await IsUsernameValidAsync(usernameValidDto);
        if (!isUsernameExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(usernameValidDto.UserName), usernameValidDto.UserName);
        }
    }
}