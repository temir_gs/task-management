using System.Text.RegularExpressions;
using FluentValidation;
using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Users.Application.Validators;

public class EmailValidator : AbstractValidator<EmailValidDto>
{
    public EmailValidator()
    {
        RuleFor(e => e.Email)
            .NotNull().WithMessage("Email is required!")
            .NotEmpty().WithMessage("Email is required!")
            .Must(CheckEmailAddressCharacters).WithMessage("Email address is not valid!");
    }

    private bool CheckEmailAddressCharacters(string emailAddress)
    {
        return String.IsNullOrEmpty(emailAddress) ||
               Regex.Matches(emailAddress, @"^[^@\s]+@[^@\s]+\.[^@\s]+$").Count > 0;
    }
}