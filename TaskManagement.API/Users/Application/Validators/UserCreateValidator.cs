using FluentValidation;
using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Users.Application.Validators;

public class UserCreateValidator : AbstractValidator<UserCreateDto>
{
    public UserCreateValidator()
    {
        RuleFor(e => e.Password)
            .NotNull().WithMessage("Password is required!")
            .NotEmpty().WithMessage("Password is required!")
            .Length(6, 100).WithMessage("Password's length must be between 6 and 100 characters");
    }
}