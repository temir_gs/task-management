using FluentValidation;
using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Users.Application.Validators;

public class UserUpdateValidator : AbstractValidator<UserUpdateDto>
{
    public UserUpdateValidator()
    {
        RuleFor(e => e.Id)
            .GreaterThan(0).WithMessage("Id is required!");

        RuleFor(e => e.UserName)
            .NotNull().WithMessage("Username is required!")
            .NotEmpty().WithMessage("Username is required!")
            .Length(6, 100).WithMessage("Password's length must be between 6 and 100 characters");
    }
}