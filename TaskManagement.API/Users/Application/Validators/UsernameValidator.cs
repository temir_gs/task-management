using FluentValidation;
using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Users.Application.Validators;

public class UsernameValidator : AbstractValidator<UsernameValidDto>
{
    public UsernameValidator()
    {
        RuleFor(e => e.UserName)
            .NotNull().WithMessage("Username is required!")
            .NotEmpty().WithMessage("Username is required!");
    }
}