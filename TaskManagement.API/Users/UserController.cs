using Microsoft.AspNetCore.Mvc;
using TaskManagement.API.Common.Controllers;
using TaskManagement.API.CustomAttributes;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Users.Application.Dtos;
using TaskManagement.API.Users.Application.Services;

namespace TaskManagement.API.Users;

public class UserController : BaseController
{
    private readonly int? OrganizationId;
    private readonly IUserService _userService;

    public UserController(IUserService userService,
        IHttpContextAccessor httpContextAccessor)
    {
        _userService = userService;
        OrganizationId = AuthHelper.GetOrganizationIdFromContext(httpContextAccessor.HttpContext);
    }

    [HttpGet("users")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> GetAllAsync()
    {
        return Ok(await _userService.GetAllAsync(OrganizationId.Value));
    }

    [HttpGet("users/{id}")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> GetByIdAsync(int id)
    {
        return Ok(await _userService.GetByIdAsync(id, OrganizationId.Value));
    }


    [HttpPost("users")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> CreateAsync([FromBody] UserCreateDto userCreateDto)
    {
        await _userService.CreateAsync(userCreateDto);
        return Ok();
    }

    [HttpPut("users")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> UpdateAsync([FromBody] UserUpdateDto updateUserDto)
    {
        await _userService.UpdateAsync(updateUserDto);
        return Ok();
    }

    [HttpDelete("users/{id}")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> DeleteAsync(int id)
    {
        await _userService.DeleteAsync(id, OrganizationId.Value);
        return Ok();
    }

    [HttpGet("users/email/is-valid")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> IsEmailValidAsync([FromQuery] EmailValidDto emailValidDto)
    {
        return Ok(await _userService.IsEmailValidAsync(emailValidDto));
    }

    [HttpGet("users/username/is-valid")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> IsUsernameValidAsync([FromQuery] UsernameValidDto usernameValidDto)
    {
        return Ok(await _userService.IsUsernameValidAsync(usernameValidDto));
    }
}