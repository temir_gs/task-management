using TaskManagement.API.Common.Entities;
using TaskManagement.API.Organizations.Domain.Models;

namespace TaskManagement.API.Users.Domain.Models;

public class User : BaseEntity<int>, IOrganization
{
    public string UserName { get; set; }
    public string PasswordHash { get; set; }
    public string SaltHash { get; set; }
    public string Email { get; set; }
    public bool IsSuperAdmin { get; set; }

    public Organization Organization { get; set; }
    public ICollection<UserTask> UserTasks { get; set; }
    public int OrganizationId { get; set; }
}