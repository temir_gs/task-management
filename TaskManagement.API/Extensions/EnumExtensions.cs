using System.ComponentModel;

namespace TaskManagement.API.Extensions;

public static class EnumExtensions
{
    public static string GetEnumDescription(this Enum value) =>
        value.GetType().GetField(value.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false) is
            DescriptionAttribute[] customAttributes &&
        ((IEnumerable<DescriptionAttribute>)customAttributes).Any<DescriptionAttribute>()
            ? ((IEnumerable<DescriptionAttribute>)customAttributes).First<DescriptionAttribute>().Description
            : value.ToString();
}