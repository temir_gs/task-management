using AutoMapper;
using FluentValidation;
using TaskManagement.API.Accounts.Application.Services;
using TaskManagement.API.Helpers.CacheHelpers;
using TaskManagement.API.Helpers.CertificateHelpers;
using TaskManagement.API.Helpers.CustomAuthenticationHelpers;
using TaskManagement.API.Helpers.EmailHelpers;
using TaskManagement.API.Helpers.EmailHelpers.Settings;
using TaskManagement.API.Helpers.JwtHelpers;
using TaskManagement.API.Organizations.Application.Dtos;
using TaskManagement.API.Organizations.Application.Mapper;
using TaskManagement.API.Organizations.Application.Services;
using TaskManagement.API.Organizations.Application.Validators;
using TaskManagement.API.Tasks.Application.Dtos;
using TaskManagement.API.Tasks.Application.Mapper;
using TaskManagement.API.Tasks.Application.Services;
using TaskManagement.API.Tasks.Application.Validators;
using TaskManagement.API.Users.Application.Dtos;
using TaskManagement.API.Users.Application.Mapper;
using TaskManagement.API.Users.Application.Services;
using TaskManagement.API.Users.Application.Validators;

namespace TaskManagement.API.Extensions;

public static class ServiceExtensions
{
    public static void RegisterCustomServices(this IServiceCollection services, IConfiguration configuration)
    {
        RegisterMappers(services);
        RegisterValidators(services);
        RegisterServices(services, configuration);
        RegisterHelpers(services, configuration);
    }

    private static void RegisterHelpers(IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<IJwtHelper, JwtHelper>();
        services.AddTransient<IEmailHelper, EmailHelper>();
        services.AddTransient<ICacheHelper, CacheHelper>();
        services.AddTransient<ICertificateHelper, CertificateHelper>();
        services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

        services.Configure<EmailConfiguration>(configuration.GetSection("EmailConfiguration"));
        services.AddAuthentication("Bearer")
            .AddScheme<BasicAuthenticationOptions, CustomAuthenticationHandler>("Bearer", null);

        services.AddStackExchangeRedisCache(options =>
        {
            options.Configuration = configuration.GetValue<string>("CacheSettings:Host");
            options.InstanceName = configuration.GetValue<string>("CacheSettings:InstanceName");
        });
    }

    private static void RegisterServices(IServiceCollection services, IConfiguration configuration)
    {
        services.AddTransient<ITaskService, TaskService>();
        services.AddTransient<IUserService, UserService>();
        services.AddTransient<IAccountService, AccountService>();
        services.AddTransient<IOrganizationService, OrganizationService>();
    }

    private static void RegisterMappers(IServiceCollection services)
    {
        MapperConfiguration mappingConfig = new MapperConfiguration(mc =>
        {
            mc.AddProfile(new TaskMappingProfile());
            mc.AddProfile(new UserMappingProfile());
            mc.AddProfile(new OrganizationMappingProfile());
        });

        IMapper mapper = mappingConfig.CreateMapper();
        services.AddSingleton(mapper);
    }

    private static void RegisterValidators(IServiceCollection services)
    {
        #region Tasks

        services.AddTransient<IValidator<TaskCreateDto>, TaskCreateValidator>();
        services.AddTransient<IValidator<TaskUpdateDto>, TaskUpdateValidator>();

        #endregion

        #region Users

        services.AddTransient<IValidator<UserCreateDto>, UserCreateValidator>();
        services.AddTransient<IValidator<UserUpdateDto>, UserUpdateValidator>();
        services.AddTransient<IValidator<EmailValidDto>, EmailValidator>();
        services.AddTransient<IValidator<UsernameValidDto>, UsernameValidator>();

        #endregion

        #region Organizations

        services.AddTransient<IValidator<OrganizationCreateDto>, OrganizationCreateValidator>();
        services.AddTransient<IValidator<OrganizationUpdateDto>, OrganizationUpdateValidator>();
        services.AddTransient<IValidator<OrganizationNameValidDto>, OrganizationNameValidator>();

        #endregion
    }
}