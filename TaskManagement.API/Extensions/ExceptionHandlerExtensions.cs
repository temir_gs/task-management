using System.Text.Json;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using TaskManagement.API.Common.Exceptions;

namespace TaskManagement.API.Extensions;

public static class ExceptionHandlerExtensions
{
    public static async Task HandleExceptionAsync(this WebApplication app)
    {
        app.Use(async (context, next) =>
        {
            try
            {
                await next(context);
            }
            catch (Exception exception)
            {
                context.Response.ContentType = "application/json";
                var (statusCode, code, message) = GetExceptionDetails(exception);

                context.Response.StatusCode = statusCode;
                await context.Response.WriteAsync(JsonSerializer.Serialize(new
                {
                    statusCode,
                    code,
                    message
                }));

                string logData = JsonSerializer.Serialize(new
                {
                    statusCode,
                    message,
                    stackTrace = exception.StackTrace
                });
                app.Logger.LogError($"Something went wrong: {logData}");
            }
        });
        await Task.CompletedTask;
    }

    private static (int, string, string?) GetExceptionDetails(Exception exception) => exception switch
    {
        NotFoundException exp => (StatusCodes.Status404NotFound, exp.Code, exception.Message),
        BadRequestException exp => (StatusCodes.Status400BadRequest, exp.Code, exception.Message),
        ValidationException => (StatusCodes.Status400BadRequest, "VALIDATION_EXCEPTION", exception.Message),
        DbUpdateException => (StatusCodes.Status500InternalServerError, "INTERNAL_SERVER_EXCEPTION",
            exception.InnerException?.Message),
        _ => (StatusCodes.Status500InternalServerError, "INTERNAL_SERVER_EXCEPTION", exception.Message)
    };
}