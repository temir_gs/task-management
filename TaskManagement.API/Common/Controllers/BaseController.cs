using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TaskManagement.API.Common.Controllers;

[Authorize]
[ApiController]
[Route("api/v1/task-management/")]
public class BaseController : Controller
{
}