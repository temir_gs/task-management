namespace TaskManagement.API.Common.Entities;

public interface IDeletionAudited : IHasDeletionTime
{
    int? DeleterUserId { get; set; }
}