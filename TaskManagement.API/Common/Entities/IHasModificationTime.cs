namespace TaskManagement.API.Common.Entities;

public interface IHasModificationTime
{
    DateTime? LastModificationTime { get; set; }
}