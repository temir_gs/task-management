namespace TaskManagement.API.Common.Entities;

public class SelectItemDto
{
    public int Id { get; set; }

    public string Value { get; set; }
}