namespace TaskManagement.API.Common.Entities;

public interface IModificationAudited : IHasModificationTime
{
    int? LastModifierUserId { get; set; }
}