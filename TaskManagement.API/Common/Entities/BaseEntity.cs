namespace TaskManagement.API.Common.Entities;

public class BaseEntity<T> : ICreationAudited, IModificationAudited, IDeletionAudited
{
    public T Id { get; set; }
    public virtual DateTime? CreationTime { get; set; }
    public virtual int? CreatorUserId { get; set; }
    public DateTime? DeletionTime { get; set; }
    public int? DeleterUserId { get; set; }
    public bool IsDeleted { get; set; } = false;
    public DateTime? LastModificationTime { get; set; }
    public int? LastModifierUserId { get; set; }
}