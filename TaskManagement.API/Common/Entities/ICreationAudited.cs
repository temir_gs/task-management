namespace TaskManagement.API.Common.Entities;

public interface ICreationAudited : IHasCreationTime
{
    int? CreatorUserId { get; set; }
}