namespace TaskManagement.API.Common.Entities;

public interface IHasCreationTime
{
    DateTime? CreationTime { get; set; }
}