namespace TaskManagement.API.Common.Entities;

public interface IOrganization
{
    int OrganizationId { get; set; }
}