namespace TaskManagement.API.Common.Entities;

public interface ISoftDelete
{
    bool IsDeleted { get; set; }
}