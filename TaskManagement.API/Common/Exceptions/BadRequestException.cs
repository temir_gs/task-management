namespace TaskManagement.API.Common.Exceptions;

public class BadRequestException : Exception
{
    private BadRequestException(string code, string message)
        : base(message)
    {
        Code = code;
    }

    public string Code { get; set; }

    public static void ThrowIfAlreadyExists(string key, object value)
    {
        Throw("BAD_REQUEST_EXCEPTION", $"{key}: |{value}| already is exists.");
    }

    public static void ThrowMessage(string message) => Throw("BAD_REQUEST_EXCEPTION", message);
    private static void Throw(string code, string message) => throw new BadRequestException(code, message);
}