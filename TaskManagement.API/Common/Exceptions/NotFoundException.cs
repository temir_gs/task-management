namespace TaskManagement.API.Common.Exceptions;

public class NotFoundException : Exception
{
    private NotFoundException(string code, string message) : base(message)
    {
        Code = code;
    }

    public virtual string Code { get; set; }

    public static void ThrowIfNull<T>(T entity, string key, object value)
    {
        if (entity != null)
            return;

        Throw("NOT_FOUND_EXCEPTION", $"No {typeof(T).Name} with {key} : |{value}| was found.");
    }

    private static void Throw(string code, string message) => throw new NotFoundException(code, message);
}