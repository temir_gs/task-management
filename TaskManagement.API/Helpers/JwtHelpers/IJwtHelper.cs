using Microsoft.AspNetCore.Authentication;

namespace TaskManagement.API.Helpers.JwtHelpers;

public interface IJwtHelper
{
    Task<string> GenerateAccessToken(int userId, int organizationId);
    Task<string> GenerateRefreshToken(int userId, int organizationId);
    Task<AuthenticateResult> ValidateRefreshToken(string refreshToken);
}