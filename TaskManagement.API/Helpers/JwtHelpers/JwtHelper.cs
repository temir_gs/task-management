using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Helpers.CacheHelpers;
using TaskManagement.API.Helpers.CertificateHelpers;

namespace TaskManagement.API.Helpers.JwtHelpers
{
    public class JwtHelper : IJwtHelper
    {
        private readonly ICacheHelper _cacheHelper;
        private readonly ICertificateHelper _certificateHelper;

        public JwtHelper(ICacheHelper cacheHelper, ICertificateHelper certificateHelper)
        {
            _cacheHelper = cacheHelper;
            _certificateHelper = certificateHelper;
        }

        public async Task<string> GenerateAccessToken(int userId, int organizationId)
        {
            string? publicKey =
                await _cacheHelper.GetAsync<string>("task-management:publicKey:accessToken", userId.ToString());
            string? privateKey =
                await _cacheHelper.GetAsync<string>("task-management:privateKey:accessToken", userId.ToString());

            if (!string.IsNullOrEmpty(publicKey) && !string.IsNullOrEmpty(privateKey))
                return GenerateToken(userId, organizationId, privateKey, DateTime.UtcNow.AddMinutes(120));

            (string publicKeyAccessToken, string privateKeyAccessToken) = _certificateHelper.GetPublicAndPrivateKey();
            await _cacheHelper.SetAsync("task-management:publicKey:accessToken", userId.ToString(),
                publicKeyAccessToken,
                TimeSpan.FromDays(7));
            await _cacheHelper.SetAsync("task-management:privateKey:accessToken", userId.ToString(),
                privateKeyAccessToken,
                TimeSpan.FromDays(7));

            return GenerateToken(userId, organizationId, privateKeyAccessToken, DateTime.UtcNow.AddMinutes(120));
        }

        public async Task<string> GenerateRefreshToken(int userId, int organizationId)
        {
            string? publicKey =
                await _cacheHelper.GetAsync<string>("task-management:publicKey:refreshToken", userId.ToString());
            string? privateKey =
                await _cacheHelper.GetAsync<string>("task-management:privateKey:refreshToken", userId.ToString());

            if (!string.IsNullOrEmpty(publicKey) && !string.IsNullOrEmpty(privateKey))
                return GenerateToken(userId, organizationId, privateKey, DateTime.UtcNow.AddDays(7));

            (string publicKeyRefreshToken, string privateKeyRefreshToken) = _certificateHelper.GetPublicAndPrivateKey();
            await _cacheHelper.SetAsync("task-management:publicKey:refreshToken", userId.ToString(),
                publicKeyRefreshToken,
                TimeSpan.FromDays(7));
            await _cacheHelper.SetAsync("task-management:privateKey:refreshToken", userId.ToString(),
                privateKeyRefreshToken,
                TimeSpan.FromDays(7));

            return GenerateToken(userId, organizationId, privateKeyRefreshToken, DateTime.UtcNow.AddDays(7));
        }

        public async Task<AuthenticateResult> ValidateRefreshToken(string refreshToken)
        {
            try
            {
                int userId = AuthHelper.GetUserIdFromToken(refreshToken);
                string? publicKey =
                    await _cacheHelper.GetAsync<string>("task-management:publicKey:refreshToken", userId.ToString());
                return AuthHelper.ValidateToken(refreshToken, publicKey, null);
            }
            catch (Exception ex)
            {
                return AuthenticateResult.Fail(ex.Message);
            }
        }

        private static string GenerateToken(int userId, int organizationId, string privateKey,
            DateTime tokenExpirationTime)
        {
            IList<Claim> claims = new List<Claim>
            {
                new("UserId", userId.ToString()),
                new("OrganizationId", organizationId.ToString())
            };

            RSA rsa = RSA.Create();
            rsa.FromXmlString(privateKey);

            return new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(
                signingCredentials: new SigningCredentials(key: new RsaSecurityKey(rsa),
                    algorithm: SecurityAlgorithms.RsaSha256),
                expires: tokenExpirationTime,
                claims: claims
            ));
        }
    }
}