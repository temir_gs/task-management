using System.Net;
using System.Net.Mail;
using Microsoft.Extensions.Options;
using TaskManagement.API.Helpers.EmailHelpers.Settings;

namespace TaskManagement.API.Helpers.EmailHelpers;

public class EmailHelper : IEmailHelper
{
    private readonly EmailConfiguration _configuration;

    public EmailHelper(IOptions<EmailConfiguration> configuration)
    {
        _configuration = configuration.Value;
    }

    public async Task SendAsync(List<string> emailAddress, string content, string subject)
    {
        SmtpClient smtpClient = new SmtpClient(_configuration.SmtpServer)
        {
            EnableSsl = true,
            Port = _configuration.Port,
            Credentials = new NetworkCredential(_configuration.Username, _configuration.Password),
        };

        MailMessage mailMessage = new MailMessage
        {
            Subject = subject,
            IsBodyHtml = true,
            Body = $"<h2>{content}</h2>",
            From = new MailAddress(_configuration.From)
        };

        emailAddress.ForEach(x => { mailMessage.To.Add(x); });

        await smtpClient.SendMailAsync(mailMessage);
    }
}