namespace TaskManagement.API.Helpers.EmailHelpers.Settings;

public class EmailConfiguration
{
    public int Port { get; set; }
    public string From { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string SmtpServer { get; set; }
}