namespace TaskManagement.API.Helpers.EmailHelpers;

public interface IEmailHelper
{
    Task SendAsync(List<string> emailAddress, string content, string subject);
}