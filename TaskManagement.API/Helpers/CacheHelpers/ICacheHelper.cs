namespace TaskManagement.API.Helpers.CacheHelpers;

public interface ICacheHelper
{
    Task<T?> GetAsync<T>(string tag, string key);
    Task SetAsync(string tag, string key, object value, TimeSpan expirationTime);
    Task RemoveAsync(string tag, string key);
}