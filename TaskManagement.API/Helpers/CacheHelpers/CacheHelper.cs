using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

namespace TaskManagement.API.Helpers.CacheHelpers;

public class CacheHelper : ICacheHelper
{
    private readonly IDistributedCache _distributedCache;

    public CacheHelper(IDistributedCache distributedCache)
    {
        _distributedCache = distributedCache;
    }

    public async Task<T?> GetAsync<T>(string tag, string key)
    {
        byte[] value = await _distributedCache.GetAsync($"{tag}:{key}");

        if (value == null) return default;

        string decodedValue = Encoding.UTF8.GetString(value);

        return JsonSerializer.Deserialize<T>(decodedValue);
    }

    public async Task SetAsync(string tag, string key, object value, TimeSpan expirationTime)
    {
        string jsonData = JsonSerializer.Serialize(value);

        byte[] encodedValue = Encoding.UTF8.GetBytes(jsonData);

        DistributedCacheEntryOptions options = new DistributedCacheEntryOptions()
            .SetSlidingExpiration(expirationTime);

        await _distributedCache.SetAsync($"{tag}:{key}", encodedValue, options);
    }

    public async Task RemoveAsync(string tag, string key)
    {
        await _distributedCache.RemoveAsync($"{tag}:{key}");
    }
}