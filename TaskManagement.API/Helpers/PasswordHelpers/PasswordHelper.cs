using System.Security.Cryptography;
using System.Text;

namespace TaskManagement.API.Helpers.PasswordHelpers;

public static class PasswordHelper
{
    public static (string passwordHash, string saltHash) CreatePasswordHash(string password)
    {
        using HMACSHA512 hmacshA512 = new HMACSHA512();
        string base64String = Convert.ToBase64String(hmacshA512.Key);
        return (Convert.ToBase64String(hmacshA512.ComputeHash(Encoding.UTF32.GetBytes(password))), base64String);
    }

    public static bool VerifyPassword(string password, byte[] passwordHash, byte[] saltHash)
    {
        using HMACSHA512 hmacshA512 = new HMACSHA512(saltHash);
        return !hmacshA512.ComputeHash(Encoding.UTF32.GetBytes(password))
            .Where((t, i) => t != passwordHash[i]).Any();
    }
}