using System.Text.Encodings.Web;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Options;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Helpers.CacheHelpers;

namespace TaskManagement.API.Helpers.CustomAuthenticationHelpers;

public class CustomAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
{
    private readonly ICacheHelper _cacheHelper;

    public CustomAuthenticationHandler(IOptionsMonitor<BasicAuthenticationOptions> options, ILoggerFactory logger,
        UrlEncoder encoder, ISystemClock clock, ICacheHelper cacheHelper) : base(options, logger,
        encoder, clock)
    {
        _cacheHelper = cacheHelper;
    }

    protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
    {
        if (!Request.Headers.ContainsKey("Authorization"))
            return AuthenticateResult.Fail("Unauthorized");

        string authorizationHeader = Request.Headers["Authorization"];
        if (string.IsNullOrEmpty(authorizationHeader))
            return AuthenticateResult.NoResult();

        if (!authorizationHeader.StartsWith("bearer", StringComparison.OrdinalIgnoreCase))
            return AuthenticateResult.Fail("Unauthorized");

        string token = authorizationHeader.Substring("bearer".Length).Trim();
        if (string.IsNullOrEmpty(token))
            return AuthenticateResult.Fail("Unauthorized");

        try
        {
            string? publicKey =
                await _cacheHelper.GetAsync<string>("task-management:publicKey:accessToken",
                    AuthHelper.GetUserIdFromToken(token).ToString());

            return AuthHelper.ValidateToken(token, publicKey, Scheme.Name);
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error:{ex.Message}: {ex.StackTrace} ");
            return AuthenticateResult.Fail(ex.Message);
        }
    }
}