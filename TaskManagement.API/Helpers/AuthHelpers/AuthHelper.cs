using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using Microsoft.AspNetCore.Authentication;
using Microsoft.IdentityModel.Tokens;

namespace TaskManagement.API.Helpers.AuthHelpers;

public static class AuthHelper
{
    public static int GetUserIdFromToken(string token)
    {
        JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        if (jwtSecurityTokenHandler.ReadToken(token) is not JwtSecurityToken jwtSecurity)
            throw new AggregateException("Token is invalid!");

        return int.Parse(jwtSecurity.Claims.FirstOrDefault(x => x.Type == "UserId")?.Value);
    }

    public static int GetOrganizationIdFromToken(string token)
    {
        JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        if (jwtSecurityTokenHandler.ReadToken(token) is not JwtSecurityToken jwtSecurity)
            throw new AggregateException("Token is invalid!");

        return int.Parse(jwtSecurity.Claims.FirstOrDefault(x => x.Type == "OrganizationId")?.Value);
    }

    public static int? GetUserIdFromContext(HttpContext? context)
    {
        if (context == null)
        {
            return null;
        }

        Claim claim = context.User.Claims.FirstOrDefault(x => x.Type == "UserId");

        return claim?.Value != null ? int.Parse(claim.Value) : null;
    }

    public static int? GetOrganizationIdFromContext(HttpContext? context)
    {
        if (context == null)
        {
            return null;
        }

        Claim claim = context.User.Claims.FirstOrDefault(x => x.Type == "OrganizationId");

        return claim?.Value != null ? int.Parse(claim.Value) : null;
    }

    public static AuthenticateResult ValidateToken(string token, string? publicKey, string schemaName)
    {
        RSA rsa = RSA.Create();
        rsa.FromXmlString(publicKey);

        JwtSecurityTokenHandler jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
        TokenValidationParameters tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new RsaSecurityKey(rsa),
            ValidateLifetime = true,
            LifetimeValidator = LifetimeValidator
        };
        ClaimsPrincipal principal =
            jwtSecurityTokenHandler.ValidateToken(token, tokenValidationParameters,
                out _);

        AuthenticationTicket ticket = new AuthenticationTicket(principal, schemaName);
        return AuthenticateResult.Success(ticket);
    }

    private static bool LifetimeValidator(DateTime? notBefore,
        DateTime? expires,
        SecurityToken securityToken,
        TokenValidationParameters validationParameters)
    {
        return expires != null && expires > DateTime.UtcNow;
    }
}