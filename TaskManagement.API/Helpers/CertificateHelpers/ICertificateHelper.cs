namespace TaskManagement.API.Helpers.CertificateHelpers;

public interface ICertificateHelper
{
    (string, string) GetPublicAndPrivateKey();
}