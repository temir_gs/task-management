using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskManagement.API.Accounts.Application.Dtos;
using TaskManagement.API.Accounts.Application.Services;
using TaskManagement.API.Common.Controllers;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Helpers.CacheHelpers;
using TaskManagement.API.Helpers.JwtHelpers;

namespace TaskManagement.API.Accounts;

public class AccountController : BaseController
{
    private readonly IJwtHelper _jwtHelper;
    private readonly ICacheHelper _cacheHelper;
    private readonly IAccountService _accountService;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public AccountController(
        IJwtHelper jwtHelper,
        ICacheHelper cacheHelper,
        IAccountService accountService,
        IHttpContextAccessor httpContextAccessor)
    {
        _jwtHelper = jwtHelper;
        _cacheHelper = cacheHelper;
        _accountService = accountService;
        _httpContextAccessor = httpContextAccessor;
    }

    [AllowAnonymous]
    [HttpPost("sign-in")]
    public async Task<IActionResult> SignInAsync([FromBody] UserSignInDto userSignInDto)
    {
        UserSignInResponseDto userSignInResponseDto = await _accountService.SignInAsync(userSignInDto);
        if (userSignInResponseDto.UserId <= 0)
        {
            return BadRequest("UserName or Password is invalid!");
        }

        await _cacheHelper.SetAsync("task-management:isSuperAdmin", userSignInResponseDto.UserId.ToString(),
            userSignInResponseDto.IsSuperAdmin, TimeSpan.FromDays(7));

        UserTokenDto userTokenDto = new UserTokenDto()
        {
            AccessToken =
                await _jwtHelper.GenerateAccessToken(userSignInResponseDto.UserId,
                    userSignInResponseDto.OrganizationId),
            RefreshToken =
                await _jwtHelper.GenerateRefreshToken(userSignInResponseDto.UserId,
                    userSignInResponseDto.OrganizationId)
        };

        return Ok(userTokenDto);
    }

    [AllowAnonymous]
    [HttpGet("refresh-sign-in")]
    public async Task<IActionResult> RefreshSignInAsync([FromQuery] string refreshToken)
    {
        if (string.IsNullOrEmpty(refreshToken))
            return BadRequest();

        int userId = AuthHelper.GetUserIdFromToken(refreshToken);
        int organizationId = AuthHelper.GetOrganizationIdFromToken(refreshToken);

        if (!_jwtHelper.ValidateRefreshToken(refreshToken).Result.Succeeded) return BadRequest();

        UserTokenDto userTokenDto = new UserTokenDto
        {
            AccessToken = await _jwtHelper.GenerateAccessToken(userId, organizationId),
            RefreshToken = await _jwtHelper.GenerateRefreshToken(userId, organizationId)
        };

        return Ok(userTokenDto);
    }

    [HttpPost("logout")]
    public async Task<IActionResult> LogOutAsync()
    {
        int userId = AuthHelper.GetUserIdFromContext(_httpContextAccessor.HttpContext).Value;

        string strUserId = userId.ToString();
        await _cacheHelper.RemoveAsync("task-management:publicKey:accessToken", strUserId);
        await _cacheHelper.RemoveAsync("task-management:privateKey:accessToken", strUserId);
        await _cacheHelper.RemoveAsync("task-management:publicKey:refreshToken", strUserId);
        await _cacheHelper.RemoveAsync("task-management:privateKey:refreshToken", strUserId);
        await _cacheHelper.RemoveAsync("task-management:isSuperAdmin", strUserId);

        return Ok();
    }
}