using Microsoft.EntityFrameworkCore;
using TaskManagement.API.Accounts.Application.Dtos;
using TaskManagement.API.ApplicationDataContext;
using TaskManagement.API.Common.Exceptions;
using TaskManagement.API.Helpers.PasswordHelpers;
using TaskManagement.API.Users.Domain.Models;

namespace TaskManagement.API.Accounts.Application.Services;

public class AccountService : IAccountService
{
    private readonly DataContext _dataContext;

    public AccountService(DataContext dataContext)
    {
        _dataContext = dataContext;
    }

    public async Task<UserSignInResponseDto> SignInAsync(UserSignInDto userSignInDto)
    {
        User? user = await _dataContext.Users.FirstOrDefaultAsync(x => x.UserName == userSignInDto.UserName);
        bool isPasswordCorrect = false;

        if (user is { IsDeleted: false })
        {
            isPasswordCorrect = PasswordHelper.VerifyPassword(userSignInDto.Password,
                Convert.FromBase64String(user.PasswordHash), Convert.FromBase64String(user.SaltHash));
        }

        if (user == null || !isPasswordCorrect)
        {
            BadRequestException.ThrowMessage("UserName or Password is incorrect!");
        }

        return new UserSignInResponseDto
        {
            UserId = user.Id,
            IsSuperAdmin = user.IsSuperAdmin,
            OrganizationId = user.OrganizationId
        };
    }
}