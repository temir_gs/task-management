using TaskManagement.API.Accounts.Application.Dtos;

namespace TaskManagement.API.Accounts.Application.Services;

public interface IAccountService
{
    Task<UserSignInResponseDto> SignInAsync(UserSignInDto userSignInDto);
}