namespace TaskManagement.API.Accounts.Application.Dtos
{
    public class UserSignInDto
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}