namespace TaskManagement.API.Accounts.Application.Dtos
{
    public class UserSignInResponseDto
    {
        public int UserId { get; set; }
        public int OrganizationId { get; set; }
        public bool IsSuperAdmin { get; set; }
    }
}