namespace TaskManagement.API.Accounts.Application.Dtos
{
    public class UserTokenDto
    {
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
    }
}