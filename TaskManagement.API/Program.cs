using Microsoft.EntityFrameworkCore;
using TaskManagement.API.ApplicationDataContext;
using TaskManagement.API.Extensions;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerSecurityRequirement();
builder.Services.RegisterCustomServices(builder.Configuration);
builder.Services.AddDbContext<DataContext>(options =>
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection")));

try
{
    Console.WriteLine("TaskManagement.API started");

    WebApplication app = builder.Build();
    AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
    using (var scope = app.Services.GetRequiredService<IServiceScopeFactory>().CreateScope())
    {
        scope.ServiceProvider.GetService<DataContext>()?.Database.Migrate();
    }

    app.SetSwagger();
    await app.HandleExceptionAsync();
    app.UseHttpsRedirection();
    app.UseAuthentication();
    app.UseAuthorization();
    app.MapControllers();
    app.Run();
}
catch (Exception exception)
{
    Console.WriteLine($"The Application failed to start because of exception: {exception}.");
}