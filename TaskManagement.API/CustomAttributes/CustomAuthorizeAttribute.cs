using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Helpers.CacheHelpers;

namespace TaskManagement.API.CustomAttributes;

[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
public class CustomIsSuperAdminAttribute : AuthorizeAttribute, IAuthorizationFilter
{
    private ICacheHelper? _cacheHelper;

    public void OnAuthorization(AuthorizationFilterContext context)
    {
        _cacheHelper = context.HttpContext.RequestServices.GetService<ICacheHelper>();
        string? userId = AuthHelper.GetUserIdFromContext(context.HttpContext).ToString();

        ClaimsPrincipal claimsPrincipal = context.HttpContext.User;
        if (claimsPrincipal.Identity is { IsAuthenticated: false })
        {
            context.Result = new UnauthorizedResult();
        }

        bool isSuperAdmin = Task
            .Run(async () => await _cacheHelper.GetAsync<bool>("task-management:isSuperAdmin", userId))
            .Result;

        if (isSuperAdmin)
        {
            return;
        }

        context.Result = new ForbidResult();
    }
}