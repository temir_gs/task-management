namespace TaskManagement.API.Organizations.Application.Dtos;

public class OrganizationDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string Address { get; set; }
}