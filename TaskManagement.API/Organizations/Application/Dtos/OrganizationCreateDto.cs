using TaskManagement.API.Users.Application.Dtos;

namespace TaskManagement.API.Organizations.Application.Dtos;

public class OrganizationCreateDto
{
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string Address { get; set; }

    public UserCreateDto User { get; set; }
}