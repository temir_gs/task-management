namespace TaskManagement.API.Organizations.Application.Dtos;

public class OrganizationUpdateDto
{
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string Address { get; set; }
}