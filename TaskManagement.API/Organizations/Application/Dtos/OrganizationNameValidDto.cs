namespace TaskManagement.API.Organizations.Application.Dtos;

public class OrganizationNameValidDto
{
    public int? Id { get; set; }
    public string Name { get; set; }
}