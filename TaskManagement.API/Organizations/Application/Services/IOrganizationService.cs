using TaskManagement.API.Organizations.Application.Dtos;

namespace TaskManagement.API.Organizations.Application.Services;

public interface IOrganizationService
{
    Task CreateAsync(OrganizationCreateDto organizationCreateDto);
    Task UpateAsync(OrganizationUpdateDto organizationUpdateDto, int id);
    Task DeleteAsync(int id);
    Task<bool> IsNameValidAsync(OrganizationNameValidDto organizationNameValidDto);
    Task<OrganizationDto> GetByIdAsync(int id);
}