using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using TaskManagement.API.ApplicationDataContext;
using TaskManagement.API.Common.Exceptions;
using TaskManagement.API.Helpers.PasswordHelpers;
using TaskManagement.API.Organizations.Application.Dtos;
using TaskManagement.API.Organizations.Domain.Models;
using TaskManagement.API.Users.Application.Dtos;
using TaskManagement.API.Users.Application.Services;
using TaskManagement.API.Users.Domain.Models;

namespace TaskManagement.API.Organizations.Application.Services;

public class OrganizationService : IOrganizationService
{
    private readonly IMapper _mapper;
    private readonly IUserService _userService;
    private readonly DataContext _dataContext;
    private readonly IValidator<OrganizationNameValidDto> _organizationNameValidator;
    private readonly IValidator<OrganizationCreateDto> _organizationCreateValidator;
    private readonly IValidator<OrganizationUpdateDto> _organizationUpdateValidator;

    public OrganizationService(DataContext dataContext, IMapper mapper,
        IValidator<OrganizationNameValidDto> userUpdateValidator,
        IValidator<OrganizationCreateDto> organizationCreateValidator,
        IValidator<OrganizationUpdateDto> organizationUpdateValidator,
        IUserService userService)
    {
        _dataContext = dataContext;
        _mapper = mapper;
        _organizationNameValidator = userUpdateValidator;
        _organizationCreateValidator = organizationCreateValidator;
        _organizationUpdateValidator = organizationUpdateValidator;
        _userService = userService;
    }

    public async Task CreateAsync(OrganizationCreateDto organizationCreateDto)
    {
        await _organizationCreateValidator.ValidateAndThrowAsync(organizationCreateDto);
        await ValidateCreateAsync(organizationCreateDto);

        Organization newOrganization = _mapper.Map<Organization>(organizationCreateDto);
        User newUser = _mapper.Map<User>(organizationCreateDto.User);

        (string passwordHash, string saltHash) = PasswordHelper.CreatePasswordHash(organizationCreateDto.User.Password);
        newUser.SaltHash = saltHash;
        newUser.PasswordHash = passwordHash;
        newUser.IsSuperAdmin = true;

        newOrganization.Users = new List<User> { newUser };

        await _dataContext.Organizations.AddAsync(newOrganization);
        await _dataContext.SaveChangesAsync();
    }

    public async Task UpateAsync(OrganizationUpdateDto organizationUpdateDto, int id)
    {
        await _organizationUpdateValidator.ValidateAndThrowAsync(organizationUpdateDto);
        await ValidateUpdateAsync(organizationUpdateDto);

        Organization? organization =
            await _dataContext.Organizations.FirstOrDefaultAsync(x => x.Id == id);

        NotFoundException.ThrowIfNull(organization, nameof(id), id);

        _mapper.Map(organizationUpdateDto, organization);

        _dataContext.Organizations.Update(organization);
        await _dataContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id)
    {
        Organization? organization =
            await _dataContext.Organizations.FirstOrDefaultAsync(x => x.Id == id);

        NotFoundException.ThrowIfNull(organization, nameof(id), id);

        _dataContext.Organizations.Remove(organization);
        await _dataContext.SaveChangesAsync();
    }

    public async Task<bool> IsNameValidAsync(OrganizationNameValidDto organizationNameValidDto)
    {
        await _organizationNameValidator.ValidateAndThrowAsync(organizationNameValidDto);

        bool isExist = await _dataContext.Organizations.AnyAsync(u =>
            u.Name == organizationNameValidDto.Name &&
            (!organizationNameValidDto.Id.HasValue || u.Id != organizationNameValidDto.Id));

        return !isExist;
    }

    public async Task<OrganizationDto> GetByIdAsync(int id)
    {
        Organization? organization = await _dataContext.Organizations.FirstOrDefaultAsync(x => x.Id == id);

        NotFoundException.ThrowIfNull(organization, nameof(id), id);

        return _mapper.Map<OrganizationDto>(organization);
    }

    private async Task ValidateCreateAsync(OrganizationCreateDto organizationCreateDto)
    {
        OrganizationNameValidDto organizationNameValidDto =
            _mapper.Map<OrganizationNameValidDto>(organizationCreateDto);
        bool isOrganizationNameExists = await IsNameValidAsync(organizationNameValidDto);
        if (!isOrganizationNameExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(organizationNameValidDto.Name),
                organizationNameValidDto.Name);
        }

        EmailValidDto emailValidDto = _mapper.Map<EmailValidDto>(organizationCreateDto.User);
        bool isEmailExists = await _userService.IsEmailValidAsync(emailValidDto);
        if (!isEmailExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(emailValidDto.Email), emailValidDto.Email);
        }

        UsernameValidDto usernameValidDto = _mapper.Map<UsernameValidDto>(organizationCreateDto.User);
        bool isUsernameExists = await _userService.IsUsernameValidAsync(usernameValidDto);
        if (!isUsernameExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(usernameValidDto.UserName), usernameValidDto.UserName);
        }
    }

    private async Task ValidateUpdateAsync(OrganizationUpdateDto organizationUpdateDto)
    {
        OrganizationNameValidDto organizationNameValidDto =
            _mapper.Map<OrganizationNameValidDto>(organizationUpdateDto);
        bool isOrganizationNameExists = await IsNameValidAsync(organizationNameValidDto);
        if (!isOrganizationNameExists)
        {
            BadRequestException.ThrowIfAlreadyExists(nameof(organizationNameValidDto.Name),
                organizationNameValidDto.Name);
        }
    }
}