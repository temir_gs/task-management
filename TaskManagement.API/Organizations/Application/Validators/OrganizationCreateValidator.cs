using System.Text.RegularExpressions;
using FluentValidation;
using TaskManagement.API.Organizations.Application.Dtos;

namespace TaskManagement.API.Organizations.Application.Validators;

public class OrganizationCreateValidator : AbstractValidator<OrganizationCreateDto>
{
    public OrganizationCreateValidator()
    {
        RuleFor(e => e.User.Email)
            .NotNull().WithMessage("Email is required!")
            .NotEmpty().WithMessage("Email is required!")
            .Must(CheckEmailAddressCharacters).WithMessage("Email address is not valid!");

        RuleFor(e => e.User.UserName)
            .NotNull().WithMessage("Username is required!")
            .NotEmpty().WithMessage("Username is required!");

        RuleFor(e => e.User.Password)
            .NotNull().WithMessage("Password is required!")
            .NotEmpty().WithMessage("Password is required!")
            .Length(6, 100).WithMessage("Password's length must be between 6 and 100 characters");

        RuleFor(e => e.Address)
            .NotNull().WithMessage("Address is required!")
            .NotEmpty().WithMessage("Address is required!");

        RuleFor(e => e.PhoneNumber)
            .NotNull().WithMessage("PhoneNumber is required!")
            .NotEmpty().WithMessage("PhoneNumber is required!")
            .Must(CheckPhoneNumberCharacters)
            .WithMessage("Phone number's length must be 9 numeric characters!")
            .Must(CheckPhoneNumberPrefix)
            .WithMessage("Phone number's prefix must start with 50, 51, 55, 70, 77 or 99 numeric characters!");
    }

    private bool CheckEmailAddressCharacters(string emailAddress)
    {
        return String.IsNullOrEmpty(emailAddress) ||
               Regex.Matches(emailAddress, @"^[^@\s]+@[^@\s]+\.[^@\s]+$").Count > 0;
    }

    private bool CheckPhoneNumberCharacters(string phoneNumber)
    {
        return String.IsNullOrEmpty(phoneNumber) ||
               Regex.Matches(phoneNumber, "^[0-9]{9}$").Count > 0;
    }

    private bool CheckPhoneNumberPrefix(string phoneNumber)
    {
        return String.IsNullOrEmpty(phoneNumber) ||
               Regex.Matches(phoneNumber, @"^(50|51|55|70|77|99)([1-9]{1})([0-9]{6})$").Count > 0;
    }
}