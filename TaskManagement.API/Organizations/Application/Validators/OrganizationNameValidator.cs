using FluentValidation;
using TaskManagement.API.Organizations.Application.Dtos;

namespace TaskManagement.API.Organizations.Application.Validators;

public class OrganizationNameValidator : AbstractValidator<OrganizationNameValidDto>
{
    public OrganizationNameValidator()
    {
        RuleFor(e => e.Name)
            .NotNull().WithMessage("Name is required!")
            .NotEmpty().WithMessage("Name is required!");
    }
}