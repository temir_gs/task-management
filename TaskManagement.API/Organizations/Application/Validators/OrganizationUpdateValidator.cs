using System.Text.RegularExpressions;
using FluentValidation;
using TaskManagement.API.Organizations.Application.Dtos;

namespace TaskManagement.API.Organizations.Application.Validators;

public class OrganizationUpdateValidator : AbstractValidator<OrganizationUpdateDto>
{
    public OrganizationUpdateValidator()
    {
        RuleFor(e => e.Address)
            .NotNull().WithMessage("Address is required!")
            .NotEmpty().WithMessage("Address is required!");

        RuleFor(e => e.PhoneNumber)
            .NotNull().WithMessage("PhoneNumber is required!")
            .NotEmpty().WithMessage("PhoneNumber is required!")
            .Must(CheckPhoneNumberCharacters)
            .WithMessage("Phone number's length must be 9 numeric characters!")
            .Must(CheckPhoneNumberPrefix)
            .WithMessage("Phone number's prefix must start with 50, 51, 55, 70, 77 or 99 numeric characters!");
    }

    private bool CheckPhoneNumberCharacters(string phoneNumber)
    {
        return String.IsNullOrEmpty(phoneNumber) ||
               Regex.Matches(phoneNumber, "^[0-9]{9}$").Count > 0;
    }

    private bool CheckPhoneNumberPrefix(string phoneNumber)
    {
        return String.IsNullOrEmpty(phoneNumber) ||
               Regex.Matches(phoneNumber, @"^(50|51|55|70|77|99)([1-9]{1})([0-9]{6})$").Count > 0;
    }
}