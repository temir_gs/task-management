using AutoMapper;
using TaskManagement.API.Organizations.Application.Dtos;
using TaskManagement.API.Organizations.Domain.Models;

namespace TaskManagement.API.Organizations.Application.Mapper;

public class OrganizationMappingProfile : Profile
{
    public OrganizationMappingProfile()
    {
        CreateMap<OrganizationCreateDto, Organization>();
        CreateMap<OrganizationCreateDto, OrganizationNameValidDto>();

        CreateMap<OrganizationUpdateDto, Organization>();
        CreateMap<OrganizationUpdateDto, OrganizationNameValidDto>();

        CreateMap<Organization, OrganizationDto>();
    }
}