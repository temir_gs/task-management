using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TaskManagement.API.Common.Controllers;
using TaskManagement.API.CustomAttributes;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Organizations.Application.Dtos;
using TaskManagement.API.Organizations.Application.Services;

namespace TaskManagement.API.Organizations;

public class OrganizationController : BaseController
{
    private readonly IOrganizationService _organizationService;
    private readonly IHttpContextAccessor _httpContextAccessor;

    public OrganizationController(IOrganizationService organizationService,
        IHttpContextAccessor httpContextAccessor)
    {
        _organizationService = organizationService;
        _httpContextAccessor = httpContextAccessor;
    }
    
    [CustomIsSuperAdmin]
    [HttpGet("organizations")]
    public async Task<IActionResult> GetByIdAsync()
    {
        int? id = AuthHelper.GetOrganizationIdFromContext(_httpContextAccessor.HttpContext);
        return Ok(await _organizationService.GetByIdAsync(id.Value));
    }

    
    [AllowAnonymous]
    [HttpPost("organizations")]
    public async Task<IActionResult> CreateAsync([FromBody] OrganizationCreateDto organizationCreateDto)
    {
        await _organizationService.CreateAsync(organizationCreateDto);
        return Ok();
    }

    [CustomIsSuperAdmin]
    [HttpPut("organizations")]
    public async Task<IActionResult> UpateAsync([FromBody] OrganizationUpdateDto organizationUpdateDto)
    {
        int? id = AuthHelper.GetOrganizationIdFromContext(_httpContextAccessor.HttpContext);
        await _organizationService.UpateAsync(organizationUpdateDto, id.Value);
        return Ok();
    }

    [CustomIsSuperAdmin]
    [HttpDelete("organizations")]
    public async Task<IActionResult> DeleteAsync()
    {
        int? id = AuthHelper.GetOrganizationIdFromContext(_httpContextAccessor.HttpContext);
        await _organizationService.DeleteAsync(id.Value);
        return Ok();
    }

    [AllowAnonymous]
    [HttpGet("organizations/name/is-valid")]
    public async Task<IActionResult> IsEmailValidAsync([FromQuery] OrganizationNameValidDto organizationNameValidDto)
    {
        return Ok(await _organizationService.IsNameValidAsync(organizationNameValidDto));
    }
}