using TaskManagement.API.Common.Entities;
using TaskManagement.API.Users.Domain.Models;

namespace TaskManagement.API.Organizations.Domain.Models;

public class Organization : BaseEntity<int>
{
    public string Name { get; set; }
    public string PhoneNumber { get; set; }
    public string Address { get; set; }

    public ICollection<User> Users { get; set; }
}