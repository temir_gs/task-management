using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using TaskManagement.API.Common.Entities;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Organizations.Domain.Models;
using TaskManagement.API.Users.Domain.Models;
using Task = TaskManagement.API.Tasks.Domain.Models.Task;
using TaskStatus = TaskManagement.API.Tasks.Domain.Enums.TaskStatus;

namespace TaskManagement.API.ApplicationDataContext;

public class DataContext : DbContext
{
    private readonly int? UserId;
    private readonly int? OrganizationId;

    public DataContext(DbContextOptions options, IHttpContextAccessor httpContextAccessor) : base(options)
    {
        UserId = AuthHelper.GetUserIdFromContext(httpContextAccessor.HttpContext);
        OrganizationId = AuthHelper.GetOrganizationIdFromContext(httpContextAccessor.HttpContext);
    }

    public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new())
    {
        foreach (var entry in ChangeTracker.Entries())
        {
            switch (entry.State)
            {
                case EntityState.Added:
                    SetOrganizationId(entry);
                    SetCreationAuditProperties(entry);
                    break;
                case EntityState.Modified:
                    SetModificationAuditProperties(entry);
                    break;
                case EntityState.Deleted:
                    CancelDeletionForSoftDelete(entry);
                    SetDeletionAuditProperties(entry);
                    break;
            }
        }

        int result = await base.SaveChangesAsync(cancellationToken).ConfigureAwait(false);
        return result;
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<Organization>(entity =>
        {
            entity.ToTable("organizations");
            entity.Property(e => e.Id).HasColumnName("id").IsRequired();
            entity.Property(e => e.Name).HasColumnName("name").IsRequired();
            entity.HasIndex(e => e.Name).IsUnique().HasDatabaseName("uk_organization_name");
            entity.Property(e => e.PhoneNumber).HasColumnName("phone_number").IsRequired();
            entity.Property(e => e.Address).HasColumnName("address").IsRequired();

            entity.Property(e => e.CreationTime).HasColumnName("creation_time");
            entity.Property(e => e.CreatorUserId).HasColumnName("creation_user_id");
            entity.Property(e => e.LastModificationTime).HasColumnName("last_modification_time");
            entity.Property(e => e.LastModifierUserId).HasColumnName("last_modifier_user_id");
            entity.Property(e => e.DeletionTime).HasColumnName("deletion_time");
            entity.Property(e => e.DeleterUserId).HasColumnName("deleter_user_id");
            entity.Property(e => e.IsDeleted).HasColumnName("is_delete").HasDefaultValue(false);
        });

        modelBuilder.Entity<User>(entity =>
        {
            entity.ToTable("users");
            entity.Property(e => e.Id).HasColumnName("id").IsRequired();
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id").IsRequired();
            entity.Property(e => e.UserName).HasColumnName("user_name").IsRequired();
            entity.HasIndex(e => e.UserName).IsUnique().HasDatabaseName("uk_user_name");
            entity.Property(e => e.PasswordHash).HasColumnName("password_hash").IsRequired();
            entity.Property(e => e.SaltHash).HasColumnName("salt_hash").IsRequired();
            entity.Property(e => e.Email).HasColumnName("email").IsRequired();
            entity.Property(e => e.IsSuperAdmin).HasColumnName("is_super_admin").HasDefaultValue(false).IsRequired();

            entity.Property(e => e.CreationTime).HasColumnName("creation_time");
            entity.Property(e => e.CreatorUserId).HasColumnName("creation_user_id");
            entity.Property(e => e.LastModificationTime).HasColumnName("last_modification_time");
            entity.Property(e => e.LastModifierUserId).HasColumnName("last_modifier_user_id");
            entity.Property(e => e.DeletionTime).HasColumnName("deletion_time");
            entity.Property(e => e.DeleterUserId).HasColumnName("deleter_user_id");
            entity.Property(e => e.IsDeleted).HasColumnName("is_delete").HasDefaultValue(false);
        });

        modelBuilder.Entity<Task>(entity =>
        {
            entity.ToTable("tasks");
            entity.Property(e => e.Id).HasColumnName("id").IsRequired();
            entity.Property(e => e.OrganizationId).HasColumnName("organization_id").IsRequired();
            entity.Property(e => e.Title).HasColumnName("title").IsRequired();
            entity.Property(e => e.Deadline).HasColumnName("deadline").IsRequired();
            entity.Property(e => e.Description).HasColumnName("description").IsRequired();
            entity.Property(e => e.Status).HasColumnName("status").HasDefaultValue(TaskStatus.TO_DO).IsRequired();

            entity.Property(e => e.CreationTime).HasColumnName("creation_time");
            entity.Property(e => e.CreatorUserId).HasColumnName("creation_user_id");
            entity.Property(e => e.LastModificationTime).HasColumnName("last_modification_time");
            entity.Property(e => e.LastModifierUserId).HasColumnName("last_modifier_user_id");
            entity.Property(e => e.DeletionTime).HasColumnName("deletion_time");
            entity.Property(e => e.DeleterUserId).HasColumnName("deleter_user_id");
            entity.Property(e => e.IsDeleted).HasColumnName("is_delete").HasDefaultValue(false);
        });

        modelBuilder.Entity<UserTask>(entity =>
        {
            entity.ToTable("user_tasks");
            entity.Property(e => e.Id).HasColumnName("id").IsRequired();
            entity.Property(e => e.UserId).HasColumnName("user_id").IsRequired();
            entity.Property(e => e.TaskId).HasColumnName("task_id").IsRequired();
        });

        modelBuilder.Entity<User>().HasQueryFilter(p => !p.IsDeleted);
        modelBuilder.Entity<Task>().HasQueryFilter(p => !p.IsDeleted);
        modelBuilder.Entity<Organization>().HasQueryFilter(p => !p.IsDeleted);
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Task> Tasks { get; set; }
    public DbSet<UserTask> UserTasks { get; set; }
    public DbSet<Organization> Organizations { get; set; }

    #region Configure Audit Properties

    protected virtual void SetCreationAuditProperties(EntityEntry entry)
    {
        if (entry.Entity is not ICreationAudited creationAudited) return;

        creationAudited.CreatorUserId = UserId;
        creationAudited.CreationTime = DateTime.Now;
    }

    protected virtual void SetModificationAuditProperties(EntityEntry entry)
    {
        if (entry.Entity is not IModificationAudited modificationAudited)
            return;

        modificationAudited.LastModifierUserId = UserId;
        modificationAudited.LastModificationTime = DateTime.Now;
    }

    protected virtual void SetDeletionAuditProperties(EntityEntry entry)
    {
        if (entry.Entity is not IDeletionAudited deletionAudited)
        {
            return;
        }

        deletionAudited.DeleterUserId = UserId;
        deletionAudited.DeletionTime = DateTime.Now;
    }

    protected virtual void CancelDeletionForSoftDelete(EntityEntry entry)
    {
        if (entry.Entity is not ISoftDelete)
        {
            return;
        }

        entry.Reload();
        entry.State = EntityState.Modified;
        ((ISoftDelete)entry.Entity).IsDeleted = true;
    }

    #endregion

    #region Set Organization Id

    protected virtual void SetOrganizationId(EntityEntry entry)
    {
        if (entry.Entity is not IOrganization organization)
        {
            return;
        }

        if (entry.Entity is User { IsSuperAdmin: true })
        {
            return;
        }

        organization.OrganizationId = OrganizationId.Value;
    }

    #endregion
}