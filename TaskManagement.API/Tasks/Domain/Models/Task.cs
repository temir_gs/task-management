using TaskManagement.API.Common.Entities;
using TaskManagement.API.Organizations.Domain.Models;
using TaskManagement.API.Users.Domain.Models;
using TaskStatus = TaskManagement.API.Tasks.Domain.Enums.TaskStatus;

namespace TaskManagement.API.Tasks.Domain.Models;

public class Task : BaseEntity<int>, IOrganization
{
    public string Title { get; set; }
    public TaskStatus Status { get; set; }
    public string Description { get; set; }
    public DateTime Deadline { get; set; }

    public int OrganizationId { get; set; }
    public Organization Organization { get; set; }
    public ICollection<UserTask> UserTasks { get; set; }
}