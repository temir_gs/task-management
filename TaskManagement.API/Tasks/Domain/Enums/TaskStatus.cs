using System.ComponentModel;

namespace TaskManagement.API.Tasks.Domain.Enums;

public enum TaskStatus
{
    [Description("To Do")]
    TO_DO,
    [Description("In Progress")]
    IN_PROGRESS,
    [Description("In Review")]
    IN_REVIEW,
    [Description("Done")]
    DONE
}