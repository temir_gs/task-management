namespace TaskManagement.API.Tasks.Application.Dtos;

public class TaskDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public TaskStatus Status { get; set; }
    public string Description { get; set; }
    public DateTime Deadline { get; set; }
}