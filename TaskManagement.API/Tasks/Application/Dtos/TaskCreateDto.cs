namespace TaskManagement.API.Tasks.Application.Dtos;

public class TaskCreateDto
{
    public string Title { get; set; }
    public string Description { get; set; }
    public DateTime Deadline { get; set; }
    public List<int> Users { get; set; }
}