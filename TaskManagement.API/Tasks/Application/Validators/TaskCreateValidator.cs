using FluentValidation;
using TaskManagement.API.Tasks.Application.Dtos;

namespace TaskManagement.API.Tasks.Application.Validators;

public class TaskCreateValidator : AbstractValidator<TaskCreateDto>
{
    public TaskCreateValidator()
    {
        RuleFor(e => e.Title)
            .NotNull().WithMessage("Title is required!")
            .NotEmpty().WithMessage("Title is required!");

        RuleFor(e => e.Description)
            .NotNull().WithMessage("Description is required!")
            .NotEmpty().WithMessage("Description is required!");

        RuleFor(e => e.Deadline)
            .NotNull().WithMessage("Deadline is required!")
            .NotEmpty().WithMessage("Deadline is required!");

        RuleFor(e => e.Users)
            .NotNull().WithMessage("Users are required!")
            .NotEmpty().WithMessage("Users are required!")
            .ForEach(e => e.Must(i => i > 0).WithMessage("Users are required!"));
    }
}