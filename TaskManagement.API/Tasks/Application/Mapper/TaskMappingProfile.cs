using AutoMapper;
using TaskManagement.API.Tasks.Application.Dtos;
using TaskManagement.API.Users.Domain.Models;
using Task = TaskManagement.API.Tasks.Domain.Models.Task;

namespace TaskManagement.API.Tasks.Application.Mapper;

public class TaskMappingProfile : Profile
{
    public TaskMappingProfile()
    {
        CreateMap<Task, TaskDto>();

        CreateMap<TaskCreateDto, Task>()
            .ForMember(s => s.UserTasks, d => d.MapFrom(e => e.Users.Select(t =>
                new UserTask
                {
                    TaskId = 0,
                    UserId = t
                })));

        CreateMap<TaskUpdateDto, Task>()
            .ForMember(src => src.UserTasks, dest => dest.Ignore());
    }
}