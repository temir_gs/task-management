using TaskManagement.API.Common.Entities;
using TaskManagement.API.Tasks.Application.Dtos;
using TaskStatus = TaskManagement.API.Tasks.Domain.Enums.TaskStatus;

namespace TaskManagement.API.Tasks.Application.Services;

public interface ITaskService
{
    Task<TaskDto> GetByIdAsync(int id, int organizationId);
    Task<IList<TaskDto>> GetAllAsync(int organizationId);
    IList<SelectItemDto> GetAllStatuses();
    Task ChangeStatusAsync(int id, TaskStatus taskStatus, int organizationId);
    Task CreateAsync(TaskCreateDto taskCreateDto);
    Task UpdateAsync(TaskUpdateDto taskUpdateDto);
    Task DeleteAsync(int id, int organizationId);
}