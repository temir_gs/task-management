using AutoMapper;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using TaskManagement.API.ApplicationDataContext;
using TaskManagement.API.Common.Entities;
using TaskManagement.API.Common.Exceptions;
using TaskManagement.API.Extensions;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Helpers.CacheHelpers;
using TaskManagement.API.Helpers.EmailHelpers;
using TaskManagement.API.Tasks.Application.Dtos;
using TaskManagement.API.Users.Domain.Models;
using TaskModel = TaskManagement.API.Tasks.Domain.Models.Task;
using TaskStatus = TaskManagement.API.Tasks.Domain.Enums.TaskStatus;

namespace TaskManagement.API.Tasks.Application.Services;

public class TaskService : ITaskService
{
    private readonly IMapper _mapper;
    private readonly DataContext _dataContext;
    private readonly ICacheHelper _cacheHelper;
    private readonly IEmailHelper _emailHelper;
    private readonly IHttpContextAccessor _httpContextAccessor;

    private readonly IValidator<TaskCreateDto> _taskCreateValidator;
    private readonly IValidator<TaskUpdateDto> _taskUpdateValidator;

    public TaskService(IMapper mapper,
        DataContext dataContext,
        IValidator<TaskCreateDto> taskCreateValidator,
        IValidator<TaskUpdateDto> taskUpdateValidator,
        ICacheHelper cacheHelper,
        IHttpContextAccessor httpContextAccessor,
        IEmailHelper emailHelper)
    {
        _mapper = mapper;
        _dataContext = dataContext;
        _taskCreateValidator = taskCreateValidator;
        _taskUpdateValidator = taskUpdateValidator;
        _cacheHelper = cacheHelper;
        _httpContextAccessor = httpContextAccessor;
        _emailHelper = emailHelper;
    }

    public async Task<TaskDto> GetByIdAsync(int id, int organizationId)
    {
        TaskModel? task = await _dataContext.Tasks
            .AsNoTracking()
            .FirstOrDefaultAsync(x => x.Id == id && x.OrganizationId == organizationId);

        NotFoundException.ThrowIfNull(task, nameof(id), id);

        return _mapper.Map<TaskDto>(task);
    }

    public async Task<IList<TaskDto>> GetAllAsync(int organizationId)
    {
        List<TaskModel> tasks = await _dataContext.Tasks.Where(x => x.OrganizationId == organizationId)
            .AsNoTracking()
            .ToListAsync();

        return _mapper.Map<IList<TaskDto>>(tasks);
    }

    public IList<SelectItemDto> GetAllStatuses()
    {
        return Enum.GetValues(typeof(TaskStatus))
            .Cast<TaskStatus>()
            .Select(e => new SelectItemDto()
            {
                Id = (int)e,
                Value = e.GetEnumDescription()
            })
            .ToList();
    }

    public async Task ChangeStatusAsync(int id, TaskStatus taskStatus, int organizationId)
    {
        TaskModel? task = await _dataContext.Tasks.Include(x => x.UserTasks)
            .FirstOrDefaultAsync(x => x.Id == id && x.OrganizationId == organizationId);

        NotFoundException.ThrowIfNull(task, nameof(id), id);

        int? userId = AuthHelper.GetUserIdFromContext(_httpContextAccessor.HttpContext);
        bool isSuperAdmin = await _cacheHelper.GetAsync<bool>("task-management:isSuperAdmin", userId.ToString());

        bool isExist = task.UserTasks.Any(x => x.UserId == userId);
        if (!isExist && isSuperAdmin)
        {
            BadRequestException.ThrowMessage("You cannot perform this operation!!!");
        }


        task.Status = taskStatus;
        _dataContext.Tasks.Update(task);
        await _dataContext.SaveChangesAsync();
    }

    public async Task CreateAsync(TaskCreateDto taskCreateDto)
    {
        //Create task and assign it to one or more users.
        await _taskCreateValidator.ValidateAndThrowAsync(taskCreateDto);
        await using IDbContextTransaction transaction = await _dataContext.Database.BeginTransactionAsync();

        try
        {
            TaskModel newTask = _mapper.Map<TaskModel>(taskCreateDto);
            newTask.Status = TaskStatus.TO_DO;

            await _dataContext.Tasks.AddAsync(newTask);
            await _dataContext.SaveChangesAsync();

            List<string> emailAddress = await _dataContext.Users
                .Where(x => taskCreateDto.Users.Contains(x.Id))
                .Select(x => x.Email)
                .ToListAsync();

            const string subject = "Assigned new task to you";
            string content = $"Title: {taskCreateDto.Title}: Description: {taskCreateDto.Description}";
            await _emailHelper.SendAsync(emailAddress, content, subject);

            await transaction.CommitAsync();
        }
        catch (Exception e)
        {
            await transaction.RollbackAsync();
            Console.WriteLine(e);
            throw;
        }
    }

    public async Task UpdateAsync(TaskUpdateDto taskUpdateDto)
    {
        await _taskUpdateValidator.ValidateAndThrowAsync(taskUpdateDto);

        TaskModel? task = await _dataContext.Tasks.Include(x => x.UserTasks)
            .FirstOrDefaultAsync(x => x.Id == taskUpdateDto.Id);

        NotFoundException.ThrowIfNull(task, nameof(taskUpdateDto.Id), taskUpdateDto.Id);

        _mapper.Map(taskUpdateDto, task);

        UpdateTaskUsers(task, taskUpdateDto.Users);

        _dataContext.Tasks.Update(task);
        await _dataContext.SaveChangesAsync();
    }

    public async Task DeleteAsync(int id, int organizationId)
    {
        TaskModel? task = await _dataContext.Tasks
            .FirstOrDefaultAsync(x => x.Id == id && x.OrganizationId == organizationId);

        NotFoundException.ThrowIfNull(task, nameof(id), id);

        _dataContext.Tasks.Remove(task);
        await _dataContext.SaveChangesAsync();
    }

    private void UpdateTaskUsers(TaskModel task, IList<int> users)
    {
        foreach (int userId in users)
        {
            var hasUserId = task.UserTasks.Any(x => x.UserId == userId);
            if (!hasUserId)
            {
                task.UserTasks.Add(new UserTask
                {
                    TaskId = task.Id,
                    UserId = userId
                });
            }
        }

        foreach (UserTask userTask in task.UserTasks)
        {
            var isUserIdSelected = users.Any(x => x == userTask.UserId);
            if (!isUserIdSelected)
            {
                _dataContext.UserTasks.Remove(userTask);
            }
        }
    }
}