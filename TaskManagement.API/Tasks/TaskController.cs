using Microsoft.AspNetCore.Mvc;
using TaskManagement.API.Common.Controllers;
using TaskManagement.API.CustomAttributes;
using TaskManagement.API.Helpers.AuthHelpers;
using TaskManagement.API.Tasks.Application.Dtos;
using TaskManagement.API.Tasks.Application.Services;
using TaskStatus = TaskManagement.API.Tasks.Domain.Enums.TaskStatus;

namespace TaskManagement.API.Tasks;

public class TaskController : BaseController
{
    private readonly int? OrganizationId;
    private readonly ITaskService _taskService;

    public TaskController(ITaskService taskService,
        IHttpContextAccessor httpContextAccessor)
    {
        _taskService = taskService;
        OrganizationId = AuthHelper.GetOrganizationIdFromContext(httpContextAccessor.HttpContext);
    }

    [HttpGet("tasks")]
    public async Task<IActionResult> GetAllAsync()
    {
        return Ok(await _taskService.GetAllAsync(OrganizationId.Value));
    }

    [HttpGet("tasks/{id}")]
    public async Task<IActionResult> GetByIdAsync(int id)
    {
        return Ok(await _taskService.GetByIdAsync(id, OrganizationId.Value));
    }

    [HttpPatch("tasks/{id}/status")]
    public async Task<IActionResult> ChangeStatusAsync(int id, TaskStatus taskStatus)
    {
        await _taskService.ChangeStatusAsync(id, taskStatus, OrganizationId.Value);
        return Ok();
    }

    [HttpGet("tasks/statuses")]
    public IActionResult GetAllStatuses()
    {
        return Ok(_taskService.GetAllStatuses());
    }

    [HttpPost("tasks")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> CreateAsync([FromBody] TaskCreateDto taskCreateDto)
    {
        await _taskService.CreateAsync(taskCreateDto);
        return Ok();
    }

    [HttpPut("tasks")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> UpdateAsync([FromBody] TaskUpdateDto taskUpdateDto)
    {
        await _taskService.UpdateAsync(taskUpdateDto);
        return Ok();
    }

    [HttpDelete("tasks/{id}")]
    [CustomIsSuperAdmin]
    public async Task<IActionResult> UpdateAsync(int id)
    {
        await _taskService.DeleteAsync(id, OrganizationId.Value);
        return Ok();
    }
}