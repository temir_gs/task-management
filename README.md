### TaskManagement REST API

This project is written in .net core 6 with C# 10.

### Docker Compose

How to run this application?

1. [Install Docker Compose](https://docs.docker.com/compose/install/)
2. Clone this repository
3. Run all containers with `docker-compose up --build -d `



This command will prepare image with name `task-management_web.api`.

Now, you can navigate your browser to http://localhost:8080/swagger/index.html to see the Swagger documentation of this project.
